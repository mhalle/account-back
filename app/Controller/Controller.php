<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 28/12/17
 * Time: 09:50
 */

namespace App\Controller;


use Slim\Http\Request;
use Slim\Http\Response;

abstract class Controller
{
    private $request;

    private $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request  =   $request;
        $this->response =   $response;
    }
}