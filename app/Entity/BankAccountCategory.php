<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 28/12/17
 * Time: 08:50
 */

namespace App\Entity;

use Doctrine\ORM\Mapping\Annotation as ORM;

/**
 * Class BankAccountCategory
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\BankAccountCategoryRepository")
 */
class BankAccountCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

}