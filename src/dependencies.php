<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Doctrine
$container['entityManager'] = function($c) {
    $settings = $c->get('settings')['doctrine'];
    $reader = new \Doctrine\Common\Annotations\AnnotationReader();
    $driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader);

    $connection = new \Doctrine\DBAL\Connection($settings['connection']);

    $config = new Doctrine\ORM\Configuration();
    $config->setProxyDir(__DIR__ . '/../var/doctrine/proxies');
    $config->setProxyNamespace('Proxies');
    $config->setHydratorDir(__DIR__ . '/../var/doctrine/hydrators');
    $config->setHydratorNamespace('Hydrators');
    $config->setDefaultDB('rinvoice');
    $config->setMetadataDriverImpl($driver);
    $config->setRetryQuery(3);
    $config->setRetryConnect(3);

    $eventManager = new \Doctrine\Common\EventManager();

    $em = \Doctrine\ORM\EntityManager::create($connection, $config, $eventManager);

    return $em;
};

//Controllers
$container['BankAccountController'] = function ($c)
{
    return new \App\Controller\BankAccountController($c['request'], $c['response']);
};