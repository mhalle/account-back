<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Controller\BankAccountController;

// Routes

// BankAccount
$app->get('/bank-account', 'BankAccountController:testMethod')->setName('bank-account.test');

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});


